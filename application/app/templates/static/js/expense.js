class Expense {
    get_expense(expense_id) {

    }

    get_expenses() {
        let expenses = []
        $.post({
            async: false,
            url: '/graphql',
            contentType: 'application/json',
            data: JSON.stringify({
                query: "query getExpenses {" +
                    "expenses {" +
                        "expenses{" +
                                "expense_id " +
                                "paid_total " +
                                "pay_date " +
                                "description " +
                                "created_at" +
                            "}" +
                        "}" +
                    "}"
            })
        }).done(function (response) {
            expenses = response['data']['expenses']['expenses']
        })

        return expenses
    }

    create_expense(paid_total, pay_date, description) {
        $.post({
            async: false,
            url: '/graphql',
            contentType: 'application/json',
            data: JSON.stringify({
                    query: "mutation createExpense {" +
                        "createExpense(" +
                        `    paid_total: ${paid_total},` +
                        `    pay_date: "${pay_date}",` +
                        `    description: "${description}"` +
                        ") {" +
                        "    success" +
                        "    errors" +
                        "    expense {" +
                        "      expense_id" +
                        "      pay_date" +
                        "      paid_total" +
                        "    }" +
                        "  }" +
                        "}",
            })
        }).done(function (response) {
            if (response['data']['createExpense']['success'] === true) {
                window.location.search += 'success=1';
            }
        })
    }

    populate_data_table() {
        let expenses = this.get_expenses()
        let data_table = $('#expenses_table').DataTable()

        data_table.clear()

        for (const expense_key in expenses) {
            let expense = expenses[expense_key]
            let expense_id = expense['expense_id']
            let paid_total = expense['paid_total']
            let pay_date = expense['pay_date']
            let description = expense['description']
            let created_at = expense['created_at']

            data_table.rows.add([
                {
                    0: paid_total,
                    1: pay_date,
                    2: description
                }
            ])
        }

        data_table.draw()
    }

    handle_expense_save() {
        let form_data = $('#the_form').serializeArray()
        let paid_total = form_data[0].value
        let pay_date = form_data[1].value
        let description = form_data[2].value

        this.create_expense(paid_total, pay_date, description)
    }
}